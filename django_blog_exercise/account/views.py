from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.contrib import messages


class Login(LoginView):
    template_name = "account/login.html"


class Logout(LoginRequiredMixin, LogoutView):
    template_name = "account/logout.html"


@login_required
def welcome(request):
    if not request.user.is_authenticated:
        context = {}

    user = request.user
    context = {"user": user}
    return render(request, 'account/welcome.html', context)


def register(request):
    if request.method == "GET":
        form = UserCreationForm()
        return render(request, 'account/registration.html', {"form": form})
    elif request.method == "POST":
        form = UserCreationForm(request.POST)

        if not form.is_valid():
            errors = [error for field_error in form.errors for error in form.errors[field_error]]

            for error in errors:
                messages.error(request, error)

            return render(request, 'account/registration.html',
                          {"form": form,
                           "error_message": errors})

        try:
            validate_password(form.cleaned_data['password1'])
        except ValidationError:
            messages.error(request, ["Invalid password."])
            return render(request, 'account/registration.html', {"form": form})
        else:
            form.save()

        messages.success(request, "You have successfully registered!")
        return HttpResponseRedirect(reverse('account:login'))

