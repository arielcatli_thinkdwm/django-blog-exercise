from django.urls import path, include
from . import views


app_name = "account"
urlpatterns = [
    path('', views.Login.as_view(), name="login"),
    path('logout/', views.Logout.as_view(), name="logout"),
    path('welcome/', views.welcome, name="welcome"),
    path('register/', views.register, name="register"),
]
